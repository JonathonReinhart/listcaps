#include <stdio.h>
#include <sys/capability.h>

#define test_cap_eff(caps, cap) do {                        \
    cap_flag_value_t val;                                   \
    if (cap_get_flag(caps, cap, CAP_EFFECTIVE, &val) == 0)  \
        if (val == CAP_SET)                                 \
            printf("%s\n", #cap);                           \
} while (0)

int main(void)
{
    cap_t caps;

    caps = cap_get_proc();

    /* POSIX-defined */
    test_cap_eff(caps, CAP_CHOWN);
    test_cap_eff(caps, CAP_DAC_OVERRIDE);
    test_cap_eff(caps, CAP_DAC_READ_SEARCH);
    test_cap_eff(caps, CAP_FOWNER);
    test_cap_eff(caps, CAP_FSETID);
    test_cap_eff(caps, CAP_KILL);
    test_cap_eff(caps, CAP_SETGID);
    test_cap_eff(caps, CAP_SETUID);

    /* Linux-defined */
#ifdef CAP_SETPCAP
    test_cap_eff(caps, CAP_SETPCAP);
#endif
#ifdef CAP_LINUX_IMMUTABLE
    test_cap_eff(caps, CAP_LINUX_IMMUTABLE);
#endif
#ifdef CAP_NET_BIND_SERVICE
    test_cap_eff(caps, CAP_NET_BIND_SERVICE);
#endif
#ifdef CAP_NET_BROADCAST
    test_cap_eff(caps, CAP_NET_BROADCAST);
#endif
#ifdef CAP_NET_ADMIN
    test_cap_eff(caps, CAP_NET_ADMIN);
#endif
#ifdef CAP_NET_RAW
    test_cap_eff(caps, CAP_NET_RAW);
#endif
#ifdef CAP_IPC_LOCK
    test_cap_eff(caps, CAP_IPC_LOCK);
#endif
#ifdef CAP_IPC_OWNER
    test_cap_eff(caps, CAP_IPC_OWNER);
#endif
#ifdef CAP_SYS_MODULE
    test_cap_eff(caps, CAP_SYS_MODULE);
#endif
#ifdef CAP_SYS_RAWIO
    test_cap_eff(caps, CAP_SYS_RAWIO);
#endif
#ifdef CAP_SYS_CHROOT
    test_cap_eff(caps, CAP_SYS_CHROOT);
#endif
#ifdef CAP_SYS_PTRACE
    test_cap_eff(caps, CAP_SYS_PTRACE);
#endif
#ifdef CAP_SYS_PACCT
    test_cap_eff(caps, CAP_SYS_PACCT);
#endif
#ifdef CAP_SYS_ADMIN
    test_cap_eff(caps, CAP_SYS_ADMIN);
#endif
#ifdef CAP_SYS_BOOT
    test_cap_eff(caps, CAP_SYS_BOOT);
#endif
#ifdef CAP_SYS_NICE
    test_cap_eff(caps, CAP_SYS_NICE);
#endif
#ifdef CAP_SYS_RESOURCE
    test_cap_eff(caps, CAP_SYS_RESOURCE);
#endif
#ifdef CAP_SYS_TIME
    test_cap_eff(caps, CAP_SYS_TIME);
#endif
#ifdef CAP_SYS_TTY_CONFIG
    test_cap_eff(caps, CAP_SYS_TTY_CONFIG);
#endif
#ifdef CAP_MKNOD
    test_cap_eff(caps, CAP_MKNOD);
#endif
#ifdef CAP_LEASE
    test_cap_eff(caps, CAP_LEASE);
#endif
#ifdef CAP_AUDIT_WRITE
    test_cap_eff(caps, CAP_AUDIT_WRITE);
#endif
#ifdef CAP_AUDIT_CONTROL
    test_cap_eff(caps, CAP_AUDIT_CONTROL);
#endif
#ifdef CAP_SETFCAP
    test_cap_eff(caps, CAP_SETFCAP);
#endif
#ifdef CAP_MAC_OVERRIDE
    test_cap_eff(caps, CAP_MAC_OVERRIDE);
#endif
#ifdef CAP_MAC_ADMIN
    test_cap_eff(caps, CAP_MAC_ADMIN);
#endif
#ifdef CAP_SYSLOG
    test_cap_eff(caps, CAP_SYSLOG);
#endif
#ifdef CAP_WAKE_ALARM
    test_cap_eff(caps, CAP_WAKE_ALARM);
#endif
#ifdef CAP_BLOCK_SUSPEND
    test_cap_eff(caps, CAP_BLOCK_SUSPEND);
#endif
#ifdef CAP_AUTIT_READ
    test_cap_eff(caps, CAP_AUDIT_READ);
#endif

    cap_free(caps);

    return 0;
}
